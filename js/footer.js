const footer = document.querySelector('#footer-area')
footer.innerHTML = `
<div class="container">
<div class="row section_gap">
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="single-footer-widget tp_widgets">
            <h4 class="footer_title large_title">Manos y hebras</h4>
            <p>
Las manos más que los ojos o cualquier otra parte del cuerpo habla de nuestro presente y nuestro  pasado.

Manos trabajadas porque nunca se quedaron quietas, manos que acariciaron penas,  secaron lágrimas y dieron fuerza.

            </p>
            <p>
Manos que se deslizan por cada milímetro de cientos de hebras para crear una prenda, un muñeco, un "algo" que siempre lleva horas de dedicación.Impaciencia por ver el resultado final. Orgullo por el reto conseguido.
</p>
        </div>
    </div>
    <div class="offset-lg-1 col-lg-2 col-md-6 col-sm-6">
        <div class="single-footer-widget tp_widgets">
            <h4 class="footer_title">Enlaces a Patrones</h4>
            <ul class="list">
            <li><a target="_blank" href="https://www.youtube.com/watch?v=qVHUMpA1H9w&ab_channel=manosyhebras">Receta jersei Bebe</a></li>
            <li><a target="_blank" href="https://www.youtube.com/watch?v=mfSGtXATS4I&ab_channel=manosyhebras">Chal Regina P: 1</a></li>
            <li><a target="_blank" href="https://www.youtube.com/watch?v=UXd271VxgKo&ab_channel=manosyhebras">Chal Regina P: 2</a></li>
            <li><a target="_blank" href="https://www.youtube.com/watch?v=qJD4qxeDqak&ab_channel=manosyhebras">Chal Regina P: 3</a></li>
                </ul>
        </div>
    </div>

    <div class="offset-lg-1 col-lg-3 col-md-6 col-sm-6">
        <div class="single-footer-widget tp_widgets">
            <h4 class="footer_title">Contactanos</h4>
            <div class="ml-40">
                <p class="sm-head">
                    <span class="fa fa-location-arrow"></span>
                    Almacén
                </p>
                <p>Plaza Jose María trenco, nº4, Valencia, España</p>

                <p class="sm-head">
                    <span class="fa fa-phone"></span>
                    Teléfono de contacto
                </p>
<a href="tel:+34655542656"><b>655.542.556</b></a>

                <p class="sm-head">
                    <span class="fa fa-envelope"></span>
                    Email
                    <a href="mailto:mariajoseortigosa@gmail.com">mariajoseortigosa@gmail.com</a>
                </p>
            </div>
        </div>
    </div>
</div>
</div>
`