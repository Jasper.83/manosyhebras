const posts =[
    {
        url: 'https://www.youtube.com/embed/qVHUMpA1H9w',
        title: 'Receta para tejer jersey básico',
        description: 'como tejer un jersey básico partiendo de las medidas tomadas a la persona o a otro jersey. minutaje<br>0.00 explicacion del video.<br>4.03 tomando medidas de otro jersey<br>08.26 pasando las medidas a un dibujo en cm.<br>0.15 muestra.<br>2.59 pasando las medidas a puntos.<br>4.26 montaje puntos elásticos 2/2<br>6.02 sisa como calcular puntos y tejerla<br>5.30 escote, como calcularlo<br>4.45 escote izquierdo.<br>5.50 escote derecho<br>1.45 costura hombro<br>.04.00 cogiendo puntos del escote para el cuello',
        tags: ['Creando patrón', 'Lana y agujas', 'Pasar el rato', 'Tejemos']
    },
    {
        url: 'https://www.youtube.com/embed/mfSGtXATS4I',
        title: 'tejijuntas chal regina 1º parte',
        description: 'Tejijuntas del grupo manos y hebras de facebook, en el primer video, explico el patrón ya que este chal se teje en dos partes, los simbolos, en dos me equivoque al explicarlos, puse la explicacion correcta en untexto,y las lanas con las que quedan bien y que podeis comprarme en esta web.<br>El patrón del chal es el regina marie está en ravelry y podeis descargarloaqui en español:<br><a target=\'_blank\'href="www.ravelry.com/patterns/library/regina-marie">Patrón del Regina-Marie</a>',
        tags: ['Patron', ' Puntos', 'Tejemos', 'Tutorial', 'Tejijuntas']
    },
    {
        url: 'https://www.youtube.com/embed/UXd271VxgKo',
        title: 'tejijuntas chal regina 2º parte',
        description: 'ejijuntas del grupo manos y hebras de facebook, en este segundo video  hago un gráfico completo de la puntilla para hacer el chal repetí 32 veces el gráfico. Podeis comprarme en esta web la lana. El patrón del chal es el regina marie está en ravelry y podeis descargarlo aqui en español:<br><a target="_blank" href="www.ravelry.com/patterns/library/regina-marie">Patrón del Regina-Marie</a>',
        tags: ['Patron', 'Puntos', 'Tejemos', 'Tutorial', 'Tejijuntas']
    },
    {
        url: 'https://www.youtube.com/embed/qJD4qxeDqak',
        title: 'tejijuntas chal regina 3º parte',
        description: 'Tejijuntas del grupo manos y hebras de facebook, en este tercer video cierro la puntilla, y empiezo el cuerpo con las vueltas cortas, tambien os digo como centrarlas. Podeis comprarme en esta web la lana. El patrón del chal es el regina marie está en ravelry y podeis descargarlo aqui en español:<br><a target="_blank" href="www.ravelry.com/patterns/library/regina-marie">Patrón del Regina-Marie</a>',
        tags: ['Patron', 'Puntos', 'Tejemos', 'Tutorial', 'Tejijuntas']
    },
    {
        url: 'https://youtu.be/EkS1hqPUAZU',
        title: 'tejijuntas chal regina 4º parte',
        description: 'Tejijuntas del grupo manos y hebras de facebook, en este cuarto video hago las vueltas de calado de arriba y el cierre i-cord. Podeis comprarme en esta web la lana. El patrón del chal es el regina marie está en ravelry y podeis descargarlo aqui en español:<br><a target="_blank" href="www.ravelry.com/patterns/library/regina-marie">Patrón del Regina-Marie</a>',
        tags: ['Patron', 'Puntos', 'Tejemos', 'Tutorial', 'Tejijuntas']
    },
    {
        url: 'https://www.youtube.com/embed/v3IH989JT68',
        title: 'Montaje cola larga o noruego',
        description: 'Guia corta para montar puntos con el estilo cola larga o noruego',
        tags: ['Puntos', 'Tejemos', 'Tutorial']
    },
    {
        url: 'https://www.youtube.com/embed/DJVikg78SYI',
        title: 'Lanas para chal',
        description: 'Descripcion de las lanas y algodones que mas usamos para hacer chales... hay mas opciones pero estas son las mas habituales.',
        tags: ['Lanas y algodones', 'Tejemos', 'Consejos']
    },
    {
        url: 'https://youtu.be/pXSLx2FKS3s',
        title: 'Técnica C2C para mystery cal greenwich de lovealy',
        description: 'Este video es solo una guia para el teijijuntas que vamos a hacer en el grupo de facebook manos y hebras, del Greenwinch mystery cal de lovelay. Los patrones y toda la información sobre el cal lo podéis encontrar en <br><a target="_blank" href="http://lovealy.co.uk/patterns/greenwich-mystery-cal-information-pack/"> Love Aly</a> aunque la web está en inglés en breve estará también en castellano. El kit para hacer la manta mes a mes y no tener que comprar un ovillo de cada color en breve estará disponible esta página espero que nos lo pasemos bien con este reto que va a durar todo el año.',
        tags: ['Pasar el rato', 'Patron', 'Puntos', 'Tejemos', 'Tutorial']
    }
]
const drawPosts = function (filter){
    const postsElement = document.querySelector('#posts')
    postsElement.innerHTML = ''
    posts.forEach((post) => {

      
      const htmlPost = `
      <article class="row blog_item">
      <div class="col-md-3">
     <div class="blog_info text-right">
        <div class="post_tag">
            ${post.tags.map(tag => `<a href="#">${tag} -</a>`).join('')}
        </div>
        
        </div>
</div>
<div class="col-md-9">
    <div class="blog_post">
    <iframe width="560" height="315" src="${post.url}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        <div class="blog_details">
        <a>
                <h2>${post.title}</h2>
            </a>
            <p>${post.description}</p>
            </div>
            </div>
            </div>
            </article>
            `
            postsElement.innerHTML += htmlPost
        })

}
drawPosts('')