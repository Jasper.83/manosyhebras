
const products = [
    {
      imgSrc: "img/product/knitPro-Waves/30901 waves 2.jpg",
      categoria: "Ganchillo",
      name: "KNITPRO, ganchillo 2.Nº",
      price: "5.00€"
    },
    {
      imgSrc: "img/product/knitPro-Waves/30903 waves 2'5.jpg",
      categoria: "Ganchillo",
      name: "KNITPRO, ganchillo 2'5.Nº",
      price: "7.20€"
    },
    {
      imgSrc: "img/product/knitPro-Waves/30905 waves 3.jpg",
      categoria: "Ganchillo",
      name: "KNITPRO, ganchillo 3.Nº",
      price: "8.70€"
    },
    {
      imgSrc: "img/product/knitPro-Waves/30907 waves 3'5.jpg",
      categoria: "Ganchillo",
      name: "KNITPRO, ganchillo 3'5.Nº",
      price: "5.00€"
    },
    {
      imgSrc: "img/product/knitPro-Waves/30909 waves 4.jpg",
      categoria: "Ganchillo",
      name: "KNITPRO, ganchillo 4.Nº",
      price: "3.00€"
    },
    {
      imgSrc: "img/product/knitPro-Waves/30910 waves 4'5.jpg",
      categoria: "Ganchillo",
      name: "KNITPRO, ganchillo 4'5.Nº",
      price: "7.00€"
    },
    {
      imgSrc: "img/product/knitPro-Waves/30911 waves 5.jpg",
      categoria: "Ganchillo",
      name: "KNITPRO, ganchillo 5.Nº",
      price: "2.20€"
    },
    {
      imgSrc: "img/product/knitPro-Waves/30912 waves 5'5.jpg",
      categoria: "Ganchillo",
      name: "KNITPRO, ganchillo 5'5.Nº",
      price: "6.80€"
    },
    {
      imgSrc: "img/product/knitPro-Waves/30913 waves 6.jpg",
      categoria: "Ganchillo",
      name: "KNITPRO, ganchillo 6.Nº",
      price: "5.20€"
    },
    {
      imgSrc: "img/product/knitPro-Waves/30914 waves 6'5.jpg",
      categoria: "Ganchillo",
      name: "KNITPRO, ganchillo 6'5.Nº",
      price: "5.80€"
    },
    {
      imgSrc: "img/product/knitPro-Waves/30915 waves 7.jpg",
      categoria: "Ganchillo",
      name: "KNITPRO, ganchillo 7.Nº",
      price: "5.90€"
    },
    {
      imgSrc: "img/product/knitPro-Waves/30916 waves 8.jpg",
      categoria: "Ganchillo",
      name: "KNITPRO, ganchillo 8.Nº",
      price: "5.80€"
    },
    {
      imgSrc: "img/product/knitPro-Waves/30917 waves 9.jpg",
      categoria: "Ganchillo",
      name: "KNITPRO, ganchillo 9.Nº",
      price: "3.75€"
    },
    {
      imgSrc: "img/product/knitPro-Waves/30918 waves 10.jpg",
      categoria: "Ganchillo",
      name: "KNITPRO, ganchillo 10.Nº",
      price: "6.60€"
    },
    {
      imgSrc: "img/product/knitPro-Waves/30919 waves 12.jpg",
      categoria: "Ganchillo",
      name: "KNITPRO, ganchillo 12.Nº",
      price: "5.90€"
    },
    {
      imgSrc: "img/product/RUBI_Accesorios/VM001-01.jpg",
      categoria: "Aguja Aluminio 40cm",
      name: "KNITPRO, Aguja Recta 2Nº",
      price: "1.10€"
    },
    {
      imgSrc: "img/product/RUBI_Accesorios/VM001-02.jpg",
      categoria: "Aguja Aluminio 40cm",
      name: "KNITPRO, Aguja Recta 3'5Nº",
      price: "1.40€"
    },
    {
      imgSrc: "img/product/RUBI_Accesorios/VM001-03.jpg",
      categoria: "Aguja Aluminio 40cm",
      name: "KNITPRO, Aguja Recta 4Nº",
      price: "1.50€"
    },
    {
      imgSrc: "img/product/RUBI_Accesorios/VM001-09.jpg",
      categoria: "Aguja Aluminio 40cm",
      name: "KNITPRO, Aguja Recta 4'5Nº",
      price: "1.80€"
    },
    {
      imgSrc: "img/product/RUBI_Accesorios/VM001-04.jpg",
      categoria: "Aguja Aluminio 40cm",
      name: "KNITPRO, Aguja Recta 5Nº",
      price: "2.00€"
    },
    {
      imgSrc: "img/product/RUBI_Accesorios/VM001-05.jpg",
      categoria: "Aguja Aluminio 40cm",
      name: "KNITPRO, Aguja Recta 6Nº",
      price: "2.30€"
    },
    {
      imgSrc: "img/product/RUBI_Accesorios/VM001-06.jpg",
      categoria: "Aguja Aluminio 40cm",
      name: "KNITPRO, Aguja Recta 7Nº",
      price: "2.50€"
    },
    {
      imgSrc: "img/product/RUBI_Accesorios/VM001-10.jpg",
      categoria: "Aguja Aluminio 40cm",
      name: "KNITPRO, Aguja Recta 8Nº",
      price: "2.70€"
    },
    {
      imgSrc: "img/product/RUBI_Accesorios/VM002-01.jpg",
      categoria: "Aguja Aluminio 40cm",
      name: "KNITPRO, Aguja Recta 9Nº",
      price: "2.20€"
    },
    {
      imgSrc: "img/product/RUBI_Accesorios/VM002-02.jpg",
      categoria: "Aguja Aluminio 40cm",
      name: "KNITPRO, Aguja Recta 10Nº",
      price: "2.30€"
    },
    {
      imgSrc: "img/product/RUBI_Accesorios/VM002-03.jpg",
      categoria: "Aguja Aluminio 40cm",
      name: "KNITPRO, Aguja Recta 12Nº",
      price: "3.30€"
    },
    {
      imgSrc: "img/product/RUBI_Accesorios/VM002-04.jpg",
      categoria: "Aguja Aluminio 40cm",
      name: "KNITPRO, Aguja Recta 15Nº",
      price: "4.80€"
    },
]
