
const header = document.querySelector('#header-area')
header.innerHTML = `
  <div class="main_menu">
    <nav class="navbar navbar-expand-lg navbar-light">
      <div class="container">
        <a class="navbar-brand logo_h" href="index.html"><img src="img/logo50e.jpeg" alt=""></a>
        <div class="navbar-collapse offset" id="navbarSupportedContent">
          <ul class="nav navbar-nav menu_nav ml-auto mr-auto">
            <li class="nav-item"><a class="nav-link" href="index.html#productos">Productos de temporada</a></li>
            <li class="nav-item submenu dropdown">
              <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Categorías</a>
              <ul class="dropdown-menu">
              <li class="nav-item submenu dropdown">
                <li class="nav-item"><a class="nav-link" href="amigurumi.html#productos">Amigurumi</a></li>
                <li class="nav-item"><a class="nav-link" href="bambino.html#productos">Bambino</a></li>
                <li class="nav-item"><a class="nav-link" href="bambino-jaspe.html#productos">Bambino Jaspe</a></li>
                <li class="nav-item"><a class="nav-link" href="merceria.html#productos">Mercería</a></li>
              </ul>
            </li>
            <li class="nav-item"><a class="nav-link" href="blog.html">Patrones</a></li>
          </ul>

          <ul class="nav-shop">
            <li class="nav-item"><a class="button button-header" href="#productos">Ver productos</a></li>
          </ul>
        </div>
      </div>
    </nav>
  </div>
`
