
const products = [
    {
        imgSrc: "img/product/bambino/bambino-001.jpg",
        categoria: "Ovillo",
        name: "RUBÍ Bambino, Blanco Puro",
        price: "3.25€"
      },
      {
        imgSrc: "img/product/bambino/bambino-003.jpg",
        categoria: "Ovillo",
        name: "RUBÍ Bambino, Rosa Pastel",
        price: "3.25€"
      },
      {
        imgSrc: "img/product/bambino/bambino-004.jpg",
        categoria: "Ovillo",
        name: "RUBÍ Bambino, Azul Pastel",
        price: "3.25€"
      },
      {
        imgSrc: "img/product/bambino/bambino-005.jpg",
        categoria: "Ovillo",
        name: "RUBÍ Bambino, Celeste",
        price: "3.25€"
      },
      {
        imgSrc: "img/product/bambino/bambino-007.jpg",
        categoria: "Ovillo",
        name: "RUBÍ Bambino, Gris Perla",
        price: "3.25€"
      },
      {
        imgSrc: "img/product/bambino/bambino-009.jpg",
        categoria: "Ovillo",
        name: "RUBÍ Bambino, Gris",
        price: "3.25€"
      },
      {
        imgSrc: "img/product/bambino/bambino-011.jpg",
        categoria: "Ovillo",
        name: "RUBÍ Bambino, Crema",
        price: "3.25€"
      }
]