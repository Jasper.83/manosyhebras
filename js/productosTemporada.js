
const products = [

  {
    imgSrc: "img/product/bambinoJaspe/bambinoJaspe-002.jpg",
    categoria: "Ovillo",
    name: "RUBÍ Bambino Jaspe, Amarillo",
    price: "3.83€"
  },
  {
    imgSrc: "img/product/bambinoJaspe/bambinoJaspe-004.jpg",
    categoria: "Ovillo",
    name: "RUBÍ Bambino Jaspe, Morado",
    price: "3.83€"
  },
  {
    imgSrc: "img/product/bambinoJaspe/bambinoJaspe-005.jpg",
    categoria: "Ovillo",
    name: "RUBÍ Bambino Jaspe, Azul",
    price: "3.83€"
  },
  {
    imgSrc: "img/product/bambinoJaspe/bambinoJaspe-006.jpg",
    categoria: "Ovillo",
    name: "RUBÍ Bambino Jaspe, Verde",
    price: "3.83€"
  },
  {
    imgSrc: "img/product/superCotton/superCotton-101.jpg",
    categoria: "Ovillo",
    name: "RUBÍ Super Cotton, Blanco",
    price: "2.00€"
  },
  {
    imgSrc: "img/product/superCotton/superCotton-102.jpg",
    categoria: "Ovillo",
    name: "RUBÍ Super Cotton, Blanco Roto",
    price: "2.00€"
  },
  {
    imgSrc: "img/product/superCotton/superCotton-530.jpg",
    categoria: "Ovillo",
    name: "RUBÍ Super Cotton, Azul",
    price: "2.00€"
  },
  {
    imgSrc: "img/product/superCotton/superCotton-540.jpg",
    categoria: "Ovillo",
    name: "RUBÍ Super Cotton, Azul Oscuro",
    price: "2.00€"
  },
  {
    imgSrc: "img/product/superCotton/superCotton-840.jpg",
    categoria: "Ovillo",
    name: "RUBÍ Super Cotton, Amarillo",
    price: "2.00€"
  },
  {
    imgSrc: "img/product/eco/eco-008.jpg",
    categoria: "Ovillo",
    name: "RUBÍ Eco, Morado",
    price: "3.50€"
  },
  {
    imgSrc: "img/product/eco/eco-012.jpg",
    categoria: "Ovillo",
    name: "RUBÍ Eco, Verde",
    price: "3.50€"
  },
  {
    imgSrc: "img/product/sunny/sunny-102.jpg",
    categoria: "Ovillo",
    name: "RUBÍ Sunny, Arena",
    price: "4.73€"
  },
  {
    imgSrc: "img/product/sunny/sunny-103.jpg",
    categoria: "Ovillo",
    name: "RUBÍ Sunny, Amarillo",
    price: "4.73€"
  },
  {
    imgSrc: "img/product/sunny/sunny-104.jpg",
    categoria: "Ovillo",
    name: "RUBÍ Sunny, Verde",
    price: "4.73€"
  },
  {
    imgSrc: "img/product/sunny/sunny-105.jpg",
    categoria: "Ovillo",
    name: "RUBÍ Sunny, Salmón",
    price: "4.73€"
  },
  {
    imgSrc: "img/product/sunny/sunny-106.jpg",
    categoria: "Ovillo",
    name: "RUBÍ Sunny, Magenta",
    price: "4.73€"
  },
  {
    imgSrc: "img/product/sunny/sunny-107.jpg",
    categoria: "Ovillo",
    name: "RUBÍ Sunny, Violeta Pastel",
    price: "4.73€"
  },
  {
    imgSrc: "img/product/sunny/sunny-108.jpg",
    categoria: "Ovillo",
    name: "RUBÍ Sunny, Violeta",
    price: "4.73€"
  },
  {
    imgSrc: "img/product/sunny/sunny-110.jpg",
    categoria: "Ovillo",
    name: "RUBÍ Sunny, Azul Cielo",
    price: "4.73€"
  },
  {
    imgSrc: "img/product/sunny/sunny-111.jpg",
    categoria: "Ovillo",
    name: "RUBÍ Sunny, Azul",
    price: "4.73€"
  },
  {
    imgSrc: "img/product/sunny/sunny-112.jpg",
    categoria: "Ovillo",
    name: "RUBÍ Sunny, Azul Noche",
    price: "4.73€"
  },
  {
    imgSrc: "img/product/bye-bye/VL030-052.jpg",
    categoria: "Ovillo",
    name: "RUBÍ Bye-bye, Rojo",
    price: "2.70€"
  },

  {
    imgSrc: "img/product/bye-bye/VL030-713.jpg",
    categoria: "Ovillo",
    name: "RUBÍ Bye-bye, Rosa Pastel",
    price: "2.70€"
  },

]
