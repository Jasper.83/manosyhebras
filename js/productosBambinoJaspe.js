
const products = [

    {
      imgSrc: "img/product/bambinoJaspe/bambinoJaspe-002.jpg",
      categoria: "Ovillo",
      name: "RUBÍ Bambino Jaspe, Amarillo",
      price: "3.83€"
    },
    {
      imgSrc: "img/product/bambinoJaspe/bambinoJaspe-004.jpg",
      categoria: "Ovillo",
      name: "RUBÍ Bambino Jaspe, Morado",
      price: "3.83€"
    },
    {
      imgSrc: "img/product/bambinoJaspe/bambinoJaspe-005.jpg",
      categoria: "Ovillo",
      name: "RUBÍ Bambino Jaspe, Azul",
      price: "3.83€"
    },
    {
      imgSrc: "img/product/bambinoJaspe/bambinoJaspe-006.jpg",
      categoria: "Ovillo",
      name: "RUBÍ Bambino Jaspe, Verde",
      price: "3.83€"
    }
]