
const products = [
  {
    imgSrc: "img/product/amigurumi/amigurumi-001.jpg",
    categoria: "Ovillo",
    name: "RUBÍ Amigurumi, Blanco",
    price: "2.20€"
  },
  {
    imgSrc: "img/product/amigurumi/amigurumi-102.jpg",
    categoria: "Ovillo",
    name: "RUBÍ Amigurumi, Crema",
    price: "2.20€"
  },
  {
    imgSrc: "img/product/amigurumi/amigurumi-431.jpg",
    categoria: "Ovillo",
    name: "RUBÍ Amigurumi, Celeste",
    price: "2.20€"
  },
  {
    imgSrc: "img/product/amigurumi/amigurumi-461.jpg",
    categoria: "Ovillo",
    name: "RUBÍ Amigurumi, Verde",
    price: "2.20€"
  },
  {
    imgSrc: "img/product/amigurumi/amigurumi-510.jpg",
    categoria: "Ovillo",
    name: "RUBÍ Amigurumi, Azul Claro",
    price: "2.20€"
  },
  {
    imgSrc: "img/product/amigurumi/amigurumi-522.jpg",
    categoria: "Ovillo",
    name: "RUBÍ Amigurumi, Azul",
    price: "2.20€"
  },
  {
    imgSrc: "img/product/amigurumi/amigurumi-600.jpg",
    categoria: "Ovillo",
    name: "RUBÍ Amigurumi, Rojo",
    price: "2.20€"
  },
  {
    imgSrc: "img/product/amigurumi/amigurumi-652.jpg",
    categoria: "Ovillo",
    name: "RUBÍ Amigurumi, Fucsia",
    price: "2.20€"
  },
  {
    imgSrc: "img/product/amigurumi/amigurumi-740.jpg",
    categoria: "Ovillo",
    name: "RUBÍ Amigurumi, Rosa",
    price: "2.20€"
  },
  {
    imgSrc: "img/product/amigurumi/amigurumi-761.jpg",
    categoria: "Ovillo",
    name: "RUBÍ Amigurumi, Malva",
    price: "2.20€"
  },
  {
    imgSrc: "img/product/amigurumi/amigurumi-810.jpg",
    categoria: "Ovillo",
    name: "RUBÍ Amigurumi, Amarillo Pastel",
    price: "2.20€"
  },
  {
    imgSrc: "img/product/amigurumi/amigurumi-820.jpg",
    categoria: "Ovillo",
    name: "RUBÍ Amigurumi, Amarillo",
    price: "2.20€"
  },
  {
    imgSrc: "img/product/amigurumi/amigurumi-840.jpg",
    categoria: "Ovillo",
    name: "RUBÍ Amigurumi, Yema",
    price: "2.20€"
  },
  {
    imgSrc: "img/product/amigurumi/amigurumi-860.jpg",
    categoria: "Ovillo",
    name: "RUBÍ Amigurumi, Carne",
    price: "2.20€"
  },
  {
    imgSrc: "img/product/amigurumi/amigurumi-890.jpg",
    categoria: "Ovillo",
    name: "RUBÍ Amigurumi, Marron",
    price: "2.20€"
  },
  {
    imgSrc: "img/product/amigurumi/amigurumi-910.jpg",
    categoria: "Ovillo",
    name: "RUBÍ Amigurumi, Gris Claro",
    price: "2.20€"
  }
]
