
const sanitize = function(string) {
  const withoutWhiteSpaces = string.replaceAll(' ', '')
  const withoutComas = withoutWhiteSpaces.replaceAll(',', '')
  const uncaseSensitve = withoutComas.toLowerCase()
  const latinized = uncaseSensitve.latinize()

  return latinized
}

const drawProducts = function(filter) {
  const productsElement = document.querySelector('#products')
  productsElement.innerHTML = ''
  const filteredProducts = products.filter((product) => {
    const sanitizedName = sanitize(product.name)

    return sanitizedName.includes(filter)
  })

  filteredProducts.forEach((product) => {
    const htmlProduct = `
      <div class="col-md-6 col-lg-4 col-xl-3">
      <div class="card text-center card-product">
        <div class="card-product__img">
          <img class="card-img" src="${product.imgSrc}" alt="">
        </div>
        <div class="card-body">
          <p>${product.categoria}</p>
          <h4 class="card-product__title"><a>${product.name}</a></h4>
          <p class="card-product__price">${product.price}</p>
        </div>
      </div>
      </div>
    `

    productsElement.innerHTML += htmlProduct
  })
}

const search = document.querySelector('#busqueda')
search.addEventListener('keyup', () => {
  const filter = search.value

  const sanitizedFilter = sanitize(filter)
  if (sanitizedFilter !== '' || filter === '') {
    drawProducts(sanitizedFilter)
  }
})

drawProducts('')
